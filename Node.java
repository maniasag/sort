
public class Node {
 private int data;
 private Node leftchild;
 private Node rightchild;

 public Node(int data){
  this.data = data;

  leftchild = null;
  rightchild = null;
 }

 public Node getLeftchild() {
  return leftchild;
 }
 public void setLeftchild(Node leftchild) {
  this.leftchild = leftchild;
 }
 public Node getRightchild() {
  return rightchild;
 }
 public void setRightchild(Node rightchild) {
  this.rightchild = rightchild;
 }
 public int getData() {
  return data;
 }
 public void setData(int data) {
  this.data = data;
 }


}