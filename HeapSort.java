import java.util.Arrays;

public class HeapSort {
public static int[] array = {5,9,4,5,12,19,11,15,13,20};  
public static int heapSize;
public static int LEFT(int i)
{
    return 2*i+1;
}

public static int RIGHT(int i)
{
   
    return 2*i+2;
}


public static void BUILD_MAX_HEAP()
{
    heapSize = array.length - 1;
    for(int i=array.length/2; i>=0;i--)
    {
        
        MAX_HEAPIFY(i);
    }
}
public static void MAX_HEAPIFY(int i)
  
{   
  int L = LEFT(i);
  int R = RIGHT(i);
  int Largest;
  int temp;
  if (L <= heapSize && array[L] > array[i]) {
    Largest = L;
  }
  else {
    Largest = i;
  }
  
  if (R <= heapSize && array[R] > array[Largest]) {
    Largest = R;
  }
      
  if (Largest != i) {
    temp = array[i];
    array[i] = array[Largest];
    array[Largest] = temp;
    MAX_HEAPIFY(Largest);
  }
}

public static void HEAPSORT()
{ 
  int temp;
  BUILD_MAX_HEAP();
  for(int i = array.length-1; i>=1;i--){
    temp = array[0];
    array[0] = array[i];
    array[i] = temp;
    heapSize--;
    MAX_HEAPIFY(0);
  }
}

public static void main(String[] args){ 
  HEAPSORT();
  for( int i = 0; i <= array.length-1; i++){
    System.out.print(array[i] + " ");
  }
}
}
    
    
    


    



