
public class BinaryTreeClass {

 private Node root;
 
 public BinaryTreeClass() {
  this.setRoot(null);
 }

 public Node getRoot() {
  return root;
 }

 public void setRoot(Node root) {
  this.root = root;
 }
 public void addNode(int data) {

  Node node1 = new Node(data);  //node to be added
  if (root == null){
   root = node1;
  }
  Node tempNode = new Node(0);

  tempNode = root;

  boolean isAdded = false;


  while(isAdded == false) {
   if(node1.getData() < tempNode.getData()){
    if(tempNode.getLeftchild()  != null) {
     tempNode = tempNode.getLeftchild();
    }
    else{
     tempNode.setLeftchild(node1);
     isAdded = true;
    }
   }
   else{
    if (tempNode.getRightchild() != null){
     tempNode = tempNode.getRightchild();
    }
    else{
     tempNode.setRightchild(node1);
     isAdded = false;
    }
   }

  }
 }
 
 public void printTree() {
  //complete this method
 }
 
 public Node search(int key){
  //complete this method, search for key, return node with matching key or null if not found
   return null;
 }
 
}