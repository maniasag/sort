 QUICKSORT(S, P, r)
 If p < r
  then q <- PARTITION(S, p, r)
  QUICKSORT(S, p, q-1)
 QUICKSORT(S, q+1, r)


PARTITION(S, p, r)
 x <- S[r]
 i <- p-1
 for j <- p to r-1
 do if S[j] <= x
  then i <- i+1
   swap S[i] <-> S[j]
 swap S[i+1] <-> S[r]
 return i+1